
public class EasterEgg {

	private StringBuilder builder = new StringBuilder();
	
	public boolean isActive() {
		
		return builder.toString().endsWith("mike") || builder.toString().endsWith("simon") ;
	}

	public void key(char key) {
		switch (key) {
		case 'm':			
			break;
		case 'i':			
			break;
		case 'k':			
			break;
		case 'e':			
			break;
		case 's':			
			break;
		case 'o':			
			break;
		case 'n':			
			break;
		default:
			builder = new StringBuilder();
			break;
		}
		builder.append(key);
		
	}

}
