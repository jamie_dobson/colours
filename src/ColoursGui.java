import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.SwingConstants;


public class ColoursGui {

	private JFrame fullScreenFrame;
	private Color[] colours = new Color[] {Color.RED, Color.BLACK, Color.GREEN, Color.YELLOW, Color.BLUE};
	private int index;
	private EasterEgg easterEgg = new EasterEgg();
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ColoursGui window = new ColoursGui();
					window.fullScreenFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ColoursGui() {
		initialize();
	}
	
	private void initialize() {
		fullScreenFrame = new JFrame();
		fullScreenFrame.setBounds(100, 100, 724, 579);
		fullScreenFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fullScreenFrame.getContentPane().setLayout(null);
		fullScreenFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Already there
	    fullScreenFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
	    fullScreenFrame.setUndecorated(true);		
	    fullScreenFrame.validate();	    
 		GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(fullScreenFrame);
	    
		final JPanel panel = new JPanel();
		fullScreenFrame.getContentPane().setPreferredSize(Toolkit.getDefaultToolkit().getScreenSize());
		fullScreenFrame.pack();
		fullScreenFrame.setResizable(false);		
		panel.setFocusable(true);
		final JLabel lblCunts = new JLabel("HORRIBLE CUNTS");
		lblCunts.setEnabled(false);
		lblCunts.setHorizontalAlignment(SwingConstants.CENTER);
		lblCunts.setFont(new Font("Lucida Grande", Font.PLAIN, 74));
		lblCunts.setBounds(74, 103, 1063, 849);
		lblCunts.setVisible(false);
		panel.add(lblCunts);
		
		panel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				lblCunts.setVisible(false);
				easterEgg.key(arg0.getKeyChar());
				if(easterEgg.isActive())
					lblCunts.setVisible(true);
				panel.setBackground(nextColour());
			}

		});
		
		panel.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {
				panel.setBackground(nextColour());
				
			}
			
			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		panel.setBounds(0, 0, screen.width, screen.height);
		fullScreenFrame.getContentPane().add(panel);
		panel.setLayout(null);
		
	
	}
	
		
	private Color nextColour() {
		if(index == colours.length)
			index = 0;
		return colours[index++];
	}
}
