import static org.junit.Assert.*;

import org.junit.Test;

public class TestEastEgg {

	private EasterEgg easterEgg = new EasterEgg();

	@Test
	public void test() {
		press('m');
		press('i');
		assertFalse(easterEgg.isActive());
		press('k');
		press('e');
		assertTrue(easterEgg.isActive());
		press('s');
		press('i');
		press('m');
		assertFalse(easterEgg.isActive());
		press('o');
		press('n');
		assertTrue(easterEgg.isActive());
	}

	private void press(char key) {
		easterEgg.key(key);
	}
}
